from boadata.core import DataObject, DataProperties
from core import cdb

class SignalObject(DataObject):
    '''A data object representing CDB signal.'''
    def __init__(self, signal_reference, gs_ref=None, node=None):
        super(SignalObject, self).__init__(node)
        self.signal_ref = signal_reference
        self.gs_ref = gs_ref or "Unknown"
        self._signal = None

    @property
    def properties(self):
        dp = DataProperties(self.signal_ref)
        dp.add(self.gs_ref, "Generic signal")
        return dp

    @property
    def title(self):
        return "%s:%d" % (self.gs_ref["generic_signal_name"], self.signal_ref["record_number"])

    @property
    def signal(self):
        '''Lazy-loaded CDBSignal object.'''
        if not self._signal:
            self._signal = cdb.get_signal(signal_reference=self.signal_ref)
        return self._signal

    def as_xy(self):
        return (self.signal.time_axis.data, self.signal.data)