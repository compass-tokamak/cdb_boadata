from core import cdb
from boadata.core import SelectableItemList
import threading

class RecordList(SelectableItemList):
    def __init__(self):
        super(RecordList, self).__init__()
        records = cdb.query("SELECT * FROM shot_database WHERE record_type='EXP' ORDER BY record_time DESC")
        for record in records:
            record_number = record["record_number"]
            self[record_number] = record


    @property
    def max_record_time(self):
        return max(record["record_time"] for record in self.values())

    def _item_title(self, record):
        return str(record["record_number"])

    def _item_full_title(self, record):
        return "%s (%s)" % (
            record["record_number"],
            record["record_time"])

    def update(self):
        RecordListUpdater(self).update()

    # TODO: override update method


class RecordListUpdater(object):
    def __init__(self, record_list, auto_select=True):
        self.record_list = record_list
        self.auto_select = auto_select

    def update(self):
        records = cdb.query("SELECT * FROM shot_database WHERE record_type='EXP' ORDER BY record_time DESC")
        for record in records:
            record_number = record["record_number"]
            if record_number not in self.record_list:
                self.record_list[record_number] = record
                if self.auto_select:
                    self.record_list.select(record_number)



