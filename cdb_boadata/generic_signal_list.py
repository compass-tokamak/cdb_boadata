from core import cdb
from boadata.core import SelectableItemList

class GenericSignalList(SelectableItemList):
    def __init__(self):
        super(GenericSignalList, self).__init__()
        generic_signals = cdb.query("SELECT * FROM generic_signals ORDER BY generic_signal_name")
        for generic_signal in generic_signals:
            generic_signal_id = generic_signal["generic_signal_id"]
            self[generic_signal_id] = generic_signal

    def _item_title(self, generic_signal):
        return generic_signal["generic_signal_name"]

    def _item_full_title(self, generic_signal):
        return "%s (generic_signal_id=%i, data_source_id=%i)" % (
            generic_signal["generic_signal_name"],
            generic_signal["generic_signal_id"],
            generic_signal["data_source_id"])