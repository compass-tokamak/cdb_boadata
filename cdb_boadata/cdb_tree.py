from PyQt4.QtGui import QAction
from PyQt4.QtCore import QString
import blinker

from boadata.core import DataNode, DataTree, DataObject 
from signal_object import SignalObject
from core import cdb
from generic_signal_list import GenericSignalList
from record_list import RecordList


class SignalNode(DataNode):
    '''A leaf node representing signal.
    '''
    # TODO: subnode HDF5 file
    # TODO: subnode for revisions
    # TODO: subnode for axes

    node_type = "Data signal"

    def __init__(self, gs, record, parent=None):
        super(SignalNode, self).__init__(parent)
        self.gs_ref = gs
        self.record = record
        self.signal_refs = cdb.get_signal_references(generic_signal_id=gs["generic_signal_id"], record_number=record)
        if not self.signal_refs:
            raise "Signal not found."

    @property
    def title(self):
        return str(self.record)

    def create_data_object(self):
        return SignalObject(self.signal_refs[0], gs_ref=self.gs_ref, node=self)


class GenericSignalNode(DataNode):
    '''A branch node representing generic signal.

    It has leaves of Signal node.'''

    node_type = "Generic signal"

    def __init__(self, generic_signal, parent=None):
        super(GenericSignalNode, self).__init__(parent)
        self.gs_id = generic_signal["generic_signal_id"]
        self.gs = generic_signal

    @property
    def title(self):
        return self.gs["generic_signal_name"]

    def add_record(self, record):
        try:
            self.add_child(SignalNode(self.gs, record, self))
        except:
            pass

    def remove_record(self, record):
        children = [child for child in self._children if child.record == record]
        if children:
            self.remove_child(children[0])        

    def load_children(self):
        for record in self.parent.records:
            self.add_record(record)


class CdbTree(DataTree):
    def __init__(self, records=[], parent=None):
        super(CdbTree, self).__init__(parent)

        self.generic_signal_list = GenericSignalList()
        
        self.record_list = RecordList()
        for record in records:
            self.record_list.select(record)
        
        self.generic_signal_list.item_selected.connect(self.on_generic_signal_selected,
            sender=self.generic_signal_list)
        self.generic_signal_list.item_deselected.connect(self.on_generic_signal_deselected,
            sender=self.generic_signal_list)
        self.record_list.item_selected.connect(self.on_record_selected,
            sender=self.record_list)
        self.record_list.item_deselected.connect(self.on_record_deselected,
            sender=self.record_list)

    def on_generic_signal_selected(self, sender, item):
        self.add_generic_signal(self.generic_signal_list[item])

    def on_generic_signal_deselected(self, sender, item):
        self.remove_generic_signal(self.generic_signal_list[item])

    def on_record_selected(self, sender, item):
        self.add_record(item)

    def on_record_deselected(self, sender, item):
        self.remove_record(item)

    @property
    def records(self):
        return set(self.record_list.selected_items)

    title = "CDB"

    menu_title = "&CDB"

    def load_children(self):
        for key in self.generic_signal_list.selected_items:
            generic_signal = self.generic_signal_list[key]
            self.add_child(GenericSignalNode(generic_signal, self))

    def add_record(self, record):
        for child in self.children:
            child.add_record(record)

    def remove_record(self, record):
        for child in self.children:
            child.remove_record(record)

    def add_generic_signal(self, generic_signal):
        self.add_child(GenericSignalNode(generic_signal, self))

    def remove_generic_signal(self, generic_signal):
        child = [child for child in self._children if child.gs == generic_signal][0]
        self.remove_child(child)


