#!/usr/bin/env python
import logging
logging.basicConfig(filename='myapp.log', level=logging.DEBUG)

import sys
from PyQt4 import QtCore, QtGui
from boadata.gui.qt import MainWindow, DataTreeModel, SelectableItemListView
from cdb_boadata.core import cdb
from cdb_boadata.cdb_tree import CdbTree

def run_app():   
    app = QtGui.QApplication(sys.argv)

    # Enable Ctrl-C in the console
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)    
    
    last_shot = cdb.last_record_number()
    shots = range(last_shot-4,last_shot+1)

    cdb_tree = CdbTree((), shots)

    model = DataTreeModel(cdb_tree)
    mw = MainWindow()
    mw.show_tree(model)

    record_list_view = SelectableItemListView(cdb_tree.record_list)
    generic_signal_list_view = SelectableItemListView(cdb_tree.generic_signal_list)

    dock_widget = QtGui.QDockWidget("Records", mw)
    dock_widget.setWidget(record_list_view)
    mw.addDockWidget(QtCore.Qt.RightDockWidgetArea, dock_widget)

    dock_widget2 = QtGui.QDockWidget("Generic signals", mw)
    dock_widget2.setWidget(generic_signal_list_view)
    mw.addDockWidget(QtCore.Qt.RightDockWidgetArea, dock_widget2)    

    mw.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    run_app()