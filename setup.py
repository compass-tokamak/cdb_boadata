#!/usr/bin/env python
from setuptools import setup, find_packages
import itertools

options = dict(
    name='cdb_boadata',
    version='0.1',
    packages=find_packages(),
    license='MIT',
    description='A Python GUI browser of CDB data based on boadata.',
    long_description=open('README.rst').read(),
    author='Jan Pipek',
    author_email='jan.pipek@gmail.com',
    # url='https://github.com/janpipek/boadata',
    install_requires = [ 'boadata' ],
    entry_points = {
        'console_scripts' : [
            'cdbboa = cdb_boadata:run_app'
        ]
    }
)

setup(**options)